---
title: Pratique
date: 2020-12-20 08:00:00
---

### Dojo & entrainements

Les entrainements sont dispensés les :

- Pour les enfants et adolescents :
  - Mercredi : 16h00 - 17h15

- Pour les adultes :
  - Lundi : 20h15 - 21h45
  - Jeudi : 20h15 - 21h45
  - Vendredi : 12h30 - 13h30 (Entrainement libre en commun avec l'association de battodo *Muga*)

Le [dojo](/about/) se situe au centre d'art-martial *Voies des Hommes* au **8 rue Gonin, 31400 TOULOUSE**
- Périphérique extérieur : sortie 18
- Périphérique intérieur : sortie 20
- Transports en communs :
  - Ligne A : Descendre à Jean-Jaures, prendre le bus L8 - Gonin et descendre à Armentières. Marcher en direction du Lidl pour le dépasser, ce sera sur votre gauche
  - Ligne B : Descendre à Rangueil, prendre le bus 23 - Jeanne d'Arc et descendre à Armentières. Marcher en direction du Lidl pour le dépasser, ce sera sur votre gauche

