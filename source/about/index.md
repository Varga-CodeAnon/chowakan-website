---
title: Le Dojo
date: 2020-12-20 07:01:57
---

<br />

![](/images/cover-dojo.jpg)

<br />

Le **Dojo 道場** est une grande salle où sont pratiqués les arts martiaux.
**Chowakan** est née en 2006, sous l’enseignement de Jean-Michel Martinuzzi et est hébergée au sein du Dojo privé  [Voies des Hommes](http://www.voiesdeshommes.com/) à Toulouse.

Ce dojo est le résultat d’une idée folle : rassembler toutes les disciplines rattachées à l’art du sabre japonais en un seul et même lieu.

Pour ce faire, Jean-Michel Martinuzzi s’est d’abord attaché à trouver un lieu, un vieux bâtiment désaffecté. Il l’a ensuite intégralement fait refaire selon les dispositions des dojos japonais.

Aujourd’hui, on franchit un portail surveillé par deux lions de pierre pour déambuler dans un jardin Zen menant au Dojo. Celui-ci dispose de vestiaires avec douches, d’une salle de repos mais ce qui nous intéresse le plus, c’est cette superbe salle de 120m² avec un parquet en pin réalisé selon des techniques inspirées des méthodes traditionnelles des dojos au Japon.

<br />

![](/images/dojo-photo.jpg)

<br />

*Chowakan* signifie "Le Lieu de l’harmonie, de l’équilibre", pour faire résonnance au fait de ne pouvoir séparer le kendo des autres facettes du sabre, comme le kenjutsu, le iaïdo ou le battodo qui sont des disciplines également pratiquées et enseignées par Jean-Michel Martinuzzi au sein de l’association **Muga** hébergée dans le même dojo.

C’est également pour faire écho à une notion très chère au kendo et dont il faut respecter l’harmonie et l’équilibre entre le corps et l’esprit : Le **KI KEN TAI no ICHI** 気剣体一致.

Enfin, ce dojo est situé à Toulouse surnommée la ville rose, dû à ses constructions en briques de terre cuite. Malgré tout, il y a un autre symbole pour cette ville, celui grâce à qui les Rois de France étaient parés de bleu. Un symbole originaire d’Asie qui se nomme *Isatis tinctoria* : la **fleur de Pastel**.

Cet autre symbole qui fera la gloire de Toulouse et un carrefour très prospère à la Renaissance forme le **Mon** 紋 de ce dojo.

<br />

![](/images/logo-petit.png)

<br />

Ce **Mon** se retrouve aussi sur nos Zekken.

<br />

![](/images/zekken.jpg)

<br />

**Chowakan** est affilié à la [FFJDA](https://www.ffjudo.com/) (Fédération Française de Judo et Disciplines Associées).