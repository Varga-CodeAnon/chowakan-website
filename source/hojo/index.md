---
title: Le Hōjō no Kata
date: 2020-12-20 07:01:57
---

![](/images/hojo-couverture.png)

<br />

L’une des principales écoles anciennes de sabre qui servit à créer le Kendo que nous connaissons s’appelle le **Kashima Shinden Jikishinkage Ryu** 鹿島神傳直心影流.

Créée sous l’Ère *Muromachi* en 1570 par **Matsumoto Bizen Mamoru Naokatsu** 松本 備前守 尚勝, ce koryu est enseigné au travers de suite de mouvements codifiés : les **kata** 形, avec un membre représentant **Uchidachi** 打太刀 l’épée d’attaque et un autre qui prend le rôle de **Shidachi** 受太刀, l’épée qui réceptionne.

le *Kashima Shinden Jikishinkage Ryu* est reconnu comme ayant une pratique et des principes qu’aucune autre école ne possède. Pour devenir un guerrier accompli, le débutant doit maîtriser trois notions.

La première est cette faculté à se déplacer toute particulière. Elle est nommée **Unpo** 運法.
*Unpo* est la façon de se déplacer lors de la pratique et peut se traduire par la « loi », « règle » ou « méthode pour le transport », « le transport » ou « déplacement ».  Cela consiste à se tenir debout face à son partenaire puis marcher très lentement vers lui en respirant  en trois étapes : inspirez, maintenez, expirez. 

Le pratiquant avance en commençant par le pied gauche et le partenaire recule en commençant par le droit.

Une attention particulière doit être portée à la façon dont le pied (talon et orteils) se place, à la façon dont les orteils saisissent le sol au contact, et à la façon dont le diaphragme et l’abdomen se contractent et s’élargissent. Pour aider à cela, les mains sont placées sur l'abdomen formant un triangle (pouces et index se touchant).

La deuxième notion est celle de **Aun no kokyu** 阿吽の呼吸.

Dans l’affrontement entre deux combattants, la respiration est utilisée afin de concentrer l’énergie à un endroit précis, le **tanden**. On cherche à tonifier le corps, à le rendre le plus optimal possible afin de résister aux assauts.
*A-Un* et le reflet des sons émis lors de la respiration. *A* quand on inspire, *Un* quand on expire. Cette pratique est l’expression de l’énergie qui entre et sort du corps.

*Aun no Kokyu* ou *souffle de l’Aun* est le concept de *Taiji* ou *Yin Yang* en **Taiji Quan** chinois 太極拳 ou *Taikyoku* en **Taikyoku Ken** japonais.

La troisième notion repose sur la pratique des katas et particulièrement sur le **Hojo no kata** 法定之形.

C’est la premier kata appris lors de l’apprentissage de ce koryu. Il fournit au pratiquant les bases, un creuset qui développe la posture, la distance, le timing, l'esprit et la puissance, et prépare le terrain pour pouvoir apprendre la stratégie et les tactiques du système.

*Hojo no kata* signifie « *principes fondamentaux* » ou « *principes de base* ». Mais il peut également être compris par « *déplacement de l’eau par la voie du sabre* ». Cette dernière traduction permettra notamment aux pratiquants d’arts martiaux chinois de comprendre le lien qui peut exister entre leur art et ce kata considéré comme très particulier.

Cette pratique demande un **Hojo bokken**, un bokken totalement droit, bien plus gros et lourd qu’un bokken classique utilisé dans les kata de Kendo d’aujourd’hui. Il consiste en un enchaînement de 4 formes portant le nom des saisons : Printemps **Haru** 春, Eté **Natsu** 夏, Automne **Aki** 秋 et Hiver **Fuyu** 冬.

Chacune de ces séquences est rythmée par un **koan** 公案,  brève anecdote absurde ou énigmatique entre un maître et son disciple ne sollicitant aucunement la logique dans le bouddhisme Zen.

**Yamaoka Tesshu** 山岡 鉄舟 a déclaré que « *la pratique du Hojo no kata est une pratique méditative aussi valable que le zazen* ». Cela justifie le fait que le but du *Hojo no kata* n’est pas seulement de maîtriser des techniques de sabre, mais aussi d’apprendre à respirer et entraîner sa propre énergie interne.

Tout au long de l’exécution de ces 4 séquences, la vitesse, l’intensité, la puissance s’éveillent pour atteindre leur sommet puis baissent pour s’endormir à la fin de l’hiver.

Cela représente le cycle de la vie, de l’enfant à la personne âgée avec l’accroissement du *Yang* puis sa décroissance pour voir l’émergence du *Yin*, comme si nous faisions le tour du *Taiji Tu*.

En cela, la maîtrise du Hojo no kata n’est pas uniquement le commencement de l’apprentissage, mais aussi le but ultime à atteindre.

En France actuellement, nous pouvons retrouver 2 formes d’enseignements.

- Tout d’abord celle de **Ota Masataka** sensei, provenant de la forme de **Kato Kanji** 加藤完治先, disciple personnel de **Yamada Jirokichi** 山田 次朗吉, 15ème shihanke du **Kashima Shinden Jikishinkage Ryu**.
- Nous pouvons également retrouver la forme enseignée par **Adolphe Schneider**, pratiquant de **karaté Shotokai** et disciple français reconnu de la forme enseignée par **Omori Sogen Roshi** 大森曹玄, moine *Rinzai Zen* du temple *Tenryu-ji* et également disciple personnel de Y**amada Jirokichi** 山田 次朗吉.