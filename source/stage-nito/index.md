---
title: Le Kendo
date: 2020-12-20 07:01:57
---

![](/images/stage-nito.png)

<br />

Chowakan aura le plaisir d'organiser un stage de **Kendo Nito** dirigé par **Julien Goullon**, Kendo 5ème Dan et Heido 5ème Dan Musashi Kai EU, accompagné d'un de ses assistants. 

Le stage aura lieu les **4 et 5 juin 2022** (week-end de la Pentecôte)  et se déroulera au **dojo Voies des hommes, 8 rue Claude Gonin, 31400 Toulouse**.

Le nombre de participants est limité à **30** personnes maximum. Le niveau minimum requis est le **2ème dan confirmé**. Les participants devront apporter leur propre matériel, à savoir **shinai 37** et **kodachi**.

Les membres qui ne souhaitent pas pratiquer Nito peuvent tout de même s'inscrire. Les oppositions sont toujours bonnes !

Les inscriptions sont d'ores et déjà ouvertes via le formulaire suivant. Elles seront réservées aux clubs de la région Occitanie jusqu’au 31 mars, pour ensuite être ouvertes à tous les autres dojo français.

### Programme 

- *Vendredi soir :* Geiko ouvert à tous les clubs, pour tous les armurés.
- *Samedi :* Première partie du stage de Kendo Nito, qui se concluera le soir par un barbecue organisé et offert par chowakan pour les participants
- *Dimanche matin jusqu’à midi :* Deuxième partie du stage Kendo Nito.


### Infos pratiques

- Tarif : **35€** par stagiaire
- Le stage se déroulera dans le respect des règles sanitaires en vigueur au mois de juin
- Les vestiaires hommes et femmes du dojo ainsi que les douches seront accessibles aux participants

On vous attend nombreux pour ce stage inédit en France !
