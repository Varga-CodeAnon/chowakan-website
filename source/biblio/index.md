---
title: Photos, vidéos, ressources diverses
date: 2020-12-20 07:01:57
---

[comment]: <> (ls /home/admin/docker_projects/chowakan/lighttpd-html/images | egrep ".jpeg|.png|.jpg|.gif"| while read -r line ; do echo "<img src=\"/images/$line\" />"; done) 

<img src="/images/beian.png" />
<img src="/images/bougie.jpg" />
<img src="/images/cover1.jpg" />
<img src="/images/cover2.jpg" />
<img src="/images/cover3.jpg" />
<img src="/images/cover4.jpg" />
<img src="/images/cover5.jpg" />
<img src="/images/cover6.jpg" />
<img src="/images/cover7.jpg" />
<img src="/images/cover8.jpg" />
<img src="/images/hojo-couverture.png" />
<img src="/images/kendo-couverture.png" />
<img src="/images/kendo.jpeg" />
<img src="/images/kendo_mptl.png" />
<img src="/images/logo.png" />
<img src="/images/logo-side.png" />
<img src="/images/pastel.jpeg" />

