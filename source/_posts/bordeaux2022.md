---
title: Stage à Bordeaux avec sensei Jean-Paul Carpentier
date: 2022/05/21 09:00:00
categories: Kendo
tags:
- stage
- passage de grade
- kendo
top: false
---

![](/images/bordeaux2022.jpg)

Le 21 – 22 Mai 2022 Stage avec Jean-Paul Carpentier
Avec passage de grade, 1er à 3ème dan.

Plus d'informations sur le site du dojo : https://www.kendobordeaux.fr/events/stage-jean-paul-carpentier/