---
title: Stage de Kendo Nito à Chowakan
date: 2022/06/04 08:00:00
categories: Kendo
tags:
- stage
- kendo
- nito
top: true
---

![](/images/stage-nito.png)

<br />

Chowakan aura le plaisir d'organiser un stage de **Kendo Nito** dirigé par **Julien Goullon**, Kendo 5ème Dan et Heido 5ème Dan Musashi Kai EU, accompagné d'un de ses assistants. 

Le stage aura lieu les **4 et 5 juin 2022** (week-end de la Pentecôte)  et se déroulera au **dojo Voies des hommes, 8 rue Claude Gonin, 31400 Toulouse**.

Le nombre de participants est limité à **30** personnes maximum. Le niveau minimum requis est le **2ème dan confirmé**. Les participants devront apporter leur propre matériel, à savoir **shinai 37** et **kodachi**.

Les inscriptions sont d'ores et déjà ouvertes via [ce formulaire](https://forms.gle/8zrFF5FZgazdd2UB8)

### Programme 

- *Vendredi soir :* Geiko ouvert à tous les clubs, pour tous les armurés.
- *Samedi :* Première partie du stage de Kendo Nito, qui se concluera le soir par un barbecue organisé et offert par chowakan pour les participants
- *Dimanche matin jusqu’à midi :* Deuxième partie du stage Kendo Nito.


### Infos pratiques

- Tarif : **35€** par stagiaire
- Le stage se déroulera dans le respect des règles sanitaires en vigueur au mois de juin
- Les vestiaires hommes et femmes du dojo ainsi que les douches seront accessibles aux participants

Pour dormir, les hôtels suivant sont à votre disposition autour du dojo :

- [B&B Hôtels](https://www.hotel-bb.com/fr/hotel/toulouse-cite-de-l-espace), *Imp. René Mouchotte, 31500 Toulouse*
- [Ibis budget Toulouse Cité de l'espace 2](https://all.accor.com/hotel/3565/index.fr.shtml), *4 RUE MAURICE HUREL,ZAC de la Grande Plaine, 31500 TOULOUSE*
- [Hôtel La Quietat](https://www.hotel-la-quietat.fr/fr/), *204 Rte de Revel, 31400 Toulouse*
- Hôtel Un oasis de bien être, *327 Av. Jean Rieux, 31500 Toulouse*
- [Hôtel F1 Toulouse Ramonville](https://all.accor.com/hotel/2228/index.fr.shtml), *Parc D'Activités Technologiques, Av. de l'Europe, 31526 Ramonville-Saint-Agne*
- [Hotel Ariane](https://hotel-ariane-toulouse.fr/), *30 Rue des Cosmonautes, 31400 Toulouse*

Plus d'informations sur la restauration à proximité à venir.

On vous attend nombreux pour ce stage rare en France !
