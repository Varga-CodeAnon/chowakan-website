---
title: Coupe des Alpes à Chambery
date: 2022/04/02 09:00:00
categories: Kendo
tags:
- competition
- passage de grade
- kendo
top: false
---

![](/images/coupe-alpes2022.jpg)

Le Dojo de Chambery organise la coupe des Alpes mais également un passage de grade en avril.

**Samedi 2 avril**
- 9h – Ouverture
- 10h – Stage (dirigé par Jean-Paul Carpentier 7 dan) 15€ par personne
- 16h30 – Passage de grade du 1er  dan au 5ème dan

**Dimanche 3 avril**
- Coupe des Alpes ! Compétition par équipe de 5 pratiquants. 5€ par personne
- 8h : ouverture et contrôle des shinais
- 9h : début des combats
- 17h : remise des récompense

Plus d'informations ici sur le site officieil du dojo : https://www.kendo-chambery.com/cda_info/