---
title: Stage à Poitiers avec sensei Walter Pomero
date: 2022/04/09 09:00:00
categories: Kendo
tags:
- stage
- kendo
top: false
---

![](/images/poitiers2022.jpg)

L’équipe du Kendo club Poitiers organise un stage avec Walter Pomero, actuel DTN de l’Italie, le 9 et 10 avril

LIEU :
Gymnase du Lycée Louis Armand, 63 Rue de la Bugellerie, 86000 Poitiers, France

𝐏𝐑𝐎𝐆𝐑𝐀𝐌𝐌𝐄 :
**Samedi** :
- 9h00 : ouverture des portes et accueil café
- 10h00 à 12h30 : entrainement
- 14h00 à 17h00 : entrainement
- 17h30 : Debrief de la journée

Restaurant le soir pour ceux qui le souhaitent

**Dimanche** :
- 9h00 : ouverture des portes et café 
- 10h00 à 13h00 : entrainement et fin du stage

𝐑𝐄𝐒𝐓𝐀𝐔𝐑𝐀𝐓𝐈𝐎𝐍 :
Le samedi et le dimanche midi des sandwichs ainsi que des desserts vous seront proposés. Faits le matin même par nos soins selon vos envies (les tarifs de la buvette sont à venir).

Stage gratuit

Lien pour les inscriptions: 

https://forms.gle/ygUVAEScwrWYDF7r5

Plus d'informations sur la page facebook : https://www.facebook.com/events/287856993279195/